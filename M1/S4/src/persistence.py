
import pickle
import os

from config import ROOT_PATH


class PicklePersistence:
    def __init__(self):
        self.phonebook = None
        self.filepath = None

    def __call__(self, phonebook):
        self.phonebook = phonebook
        self.filepath = self._get_filepath()
        return self

    def get_or_create(self):
        if os.path.exists(self.filepath):
            existing_phonebook = self.load()
            return existing_phonebook
        elif not os.path.exists(os.path.dirname(self.filepath)):
            os.makedirs(os.path.dirname(self.filepath))

    def save(self):
        with open(self.filepath, 'wb') as f:
            pickle.dump(self.phonebook, f)

    def load(self):
        with open(self.filepath, 'rb') as f:
            phonebook = pickle.load(f)
        return phonebook

    def _get_filepath(self):
        """Persistence file path"""
        folder_name = '{}_files'.format(self.phonebook.name)
        filename = '{}.pickle'.format(self.phonebook.name)
        filepath = os.path.join(ROOT_PATH, folder_name, filename)
        return filepath


class Dummy(PicklePersistence):
    def get_or_create(self, *args):
        pass

    def __call__(self, *args):
        return self

    def save(self, *args):
        pass

    def load(self, *args):
        pass

    def _get_filepath(self, *args):
        pass
