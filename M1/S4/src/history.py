import json
import os

from config import ROOT_PATH


class JSONHistory:
    def __init__(self):
        self.phonebook = None
        self.directory = None


    def __call__(self, phonebook):
        self.phonebook = phonebook
        folder_name = '{}_files'.format(self.phonebook.name)
        self.directory = os.path.join(ROOT_PATH, folder_name)
        return self


    def record(self, contact):
        """Record contact state in JSON Lines format

        Each line contains valid JSON, but as a whole it's not a valid JSON file.
        """

        if contact.id is None:
            return

        filepath = self._get_history_filepath(contact)
        with open(filepath, 'a') as f:
            json_str = contact.to_json()
            f.write(json_str + '\n')


    def read(self, contact):
        filepath = self._get_history_filepath(contact)
        items = []
        with open(filepath) as f:
            for line in f:
                item = json.loads(line)
                items.append(item)

        return items


    def _get_history_filepath(self, contact):
        filename = '{}_history.json'.format(contact.id)
        filepath = os.path.join(self.directory, filename)
        return filepath


class Dummy(JSONHistory):
    def __call__(self, phonebook):
        return self

    def record(self, contact):
        pass

    def read(self, contact):
        pass

    def _get_history_filepath(self, contact):
        pass
