import pytest

from contact import Contact
import history
from phonebook import PhoneBook
import persistence


def test_phonebook_init(default_phonebook):
    assert 3 == len(default_phonebook._contacts)


def test_phonebook_add_duplicate_raises_exception(default_phonebook):
    candidate = Contact('Urgenta')
    with pytest.raises(ValueError):
        default_phonebook.add(candidate)


def test_phonebook_find(default_phonebook):
    contact = default_phonebook.find('urgenta')
    assert Contact == type(contact)
    assert 'urgenta' == contact.name.data.lower()


@pytest.fixture
def default_phonebook():
    persistence_dummy = persistence.Dummy()
    history_dummy = history.Dummy()
    return PhoneBook(persistence=persistence_dummy,
                     history=history_dummy,
                     name='testing')
