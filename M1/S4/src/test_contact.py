import pytest

from contact import Contact
from field import Phone, Email, Name


def test_contact_init():
    contact = Contact('A')
    assert Name is type(contact.name)
    assert 'A' == contact.name.data
    assert 0 == len(contact.fields)

    with pytest.raises(ValueError):
        Contact('99')


def test_contact_add(contact):
    assert 4 == len(contact.fields)

    phone, phone2, email, email2 = contact.fields
    assert Phone is type(phone)
    assert Email is type(email)
    assert '111' == phone.data
    assert 'mobile' == phone.description
    assert 'john.doe@gmail.com' == email.data
    assert 'personal' == email.description


def test_contact_find(contact):
    phone = contact.find('222')
    assert '222' == phone.data
    assert 'landline' == phone.description

    email = contact.find('john.doe@startup.com')
    assert 'john.doe@startup.com' == email.data
    assert 'work' == email.description

    no_phone = contact.find('000')
    assert no_phone is None


def test_contact_delete(contact):
    assert 4 == len(contact.fields)
    phone = contact.find('222')
    contact.delete(phone)
    assert 3 == len(contact.fields)

    phone = contact.find('222')
    assert phone is None


def test_contact_update(contact):
    phone = contact.find('222')
    contact.update(phone, '333')
    assert '333' in [field.data for field in contact.fields]

    with pytest.raises(ValueError):
        contact.update(phone, '*/@#^&')


@pytest.fixture
def contact():
    john = Contact('John Doe')
    phone = Phone('111', 'mobile')
    phone2 = Phone('222', 'landline')
    email = Email('john.doe@gmail.com', 'personal')
    email2 = Email('john.doe@startup.com', 'work')

    john.add(phone)
    john.add(phone2)
    john.add(email)
    john.add(email2)
    return john
