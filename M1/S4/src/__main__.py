#! /usr/bin/env python3

import csv
import json
import os
import sys

from config import ROOT_PATH
from contact import Contact
from field import Email, Phone
from history import JSONHistory
from menu import Menu, Option
from persistence import PicklePersistence
from phonebook import PhoneBook


def populate(phonebook):
    filepath = os.path.join(ROOT_PATH, 'contacts.csv')
    with open(filepath) as f:
        reader = csv.DictReader(f)
        for row in reader:
            current = Contact(row['name'])
            current.add(Phone(row['phone1'], 'mobile'))
            current.add(Phone(row['phone2'], 'home'))
            current.add(Phone(row['phone3'], 'work'))
            current.add(Email(row['email'], 'personal'))

            try:
                phonebook.add(current)
            except ValueError as e:
                print(e)


class PhoneBookMenu():
    def __init__(self, phonebook):
        self.phonebook = phonebook

        self.contact_menu = Menu([
            ('title', 'Contact Options'),
            ('1', Option('Edit Contact', self.edit)),
            ('2', Option('Add Phone Number', self.add_phone)),
            ('3', Option('Add Email', self.add_email)),
            ('4', Option('Remove Field', self.remove_field)),
            ('5', Option('Delete Contact', self.delete_contact)),
            ('6', Option('History', self.show_history)),
            ('0', Option('Back', self.back)),
        ])

        self.main_menu = Menu([
            ('title', 'Phone Book ({})'.format(self.phonebook.name)),
            ('1', Option('Show Contacts', self.show_contacts)),
            ('2', Option('Search', self.search, self.contact_menu)),
            ('3', Option('Add Contact', self.add_contact)),
            ('0', Option('Exit', self.quit)),
        ])


    @staticmethod
    def search(phonebook, submenu):
        print('\nSearch Contacts')
        name = input('Name: ')

        contact = phonebook.find(name)
        if contact is None:
            print('Not found.')
            return

        print()
        print(contact)

        submenu.loop(phonebook, contact)


    @staticmethod
    def show_contacts(phonebook, **kwargs):
        for page in phonebook.paginate():
            print('\n'.join(map(str, page)))
            choice = input(':')
            if choice in set('qQ0'):
                break


    @staticmethod
    def add_contact(phonebook, **kwargs):
        print('\nAdd Contact')
        name = input('Name: ')

        try:
            contact = Contact(name)
        except ValueError as e:
            print(e)
            return

        phone_number = input('Phone number: ')
        if phone_number:
            phone_type = input('Phone type: ')

            try:
                phone = Phone(phone_number, phone_type)
                contact.add(phone)
            except ValueError as e:
                print(e)

        email = input('Email: ')
        if email:
            description = input('Email type: ')

            try:
                email = Email(email, description)
                contact.add(email)
            except ValueError as e:
                print(e)

        try:
            phonebook.add(contact)
        except ValueError as e:
            print(e)
            return

        print()
        print(contact)


    @staticmethod
    def edit(phonebook, contact, **kwargs):
        print('\nEdit Contact (Enter to keep the old value)')
        has_changed = False
        name = input('Name ({}): '.format(contact.name))
        if name != '':
            old_name = contact.name.data
            try:
                contact.update(contact.name, name)
            except ValueError as e:
                print(e)
                print()
                print(contact)
                return
            else:
                if name != old_name:
                    has_changed = True

        for field in contact.fields:
            field_data = input('{}: '.format(field.data))
            description = input('type ({}): '.format(field.description))
            old_field_data = field.data
            old_description = field.description

            try:
                contact.update(field, field_data, description)
            except ValueError as e:
                print(e)
                print()
                print(contact)
                if has_changed:
                    phonebook.history.record(contact)
                return
            else:
                if (field_data and field_data != old_field_data
                    or description and description != old_description):
                    has_changed = True

        print()
        print(contact)
        if has_changed:
            phonebook.history.record(contact)


    def add_phone(self, phonebook, contact, **kwargs):
        print('\nAdd Phone Number')
        phone_number = input('Phone number: ')
        if phone_number:
            description = input('Phone type: ')
            self.add_field(contact, Phone, phone_number, description)
            phonebook.history.record(contact)

        print()
        print(contact)


    def add_email(self, phonebook, contact, **kwargs):
        print('\nAdd Email')
        email = input('Email: ')
        if email:
            description = input('Description: ')
            self.add_field(contact, Email, email, description)
            phonebook.history.record(contact)

        print()
        print(contact)


    @staticmethod
    def add_field(contact, field_class, field_data, description):
        try:
            field = field_class(field_data, description)
        except ValueError as e:
            print(e)
            return

        try:
            contact.add(field)
        except ValueError as e:
            print(e)
            return


    @staticmethod
    def remove_field(phonebook, contact, **kwargs):
        print('\nRemove Field')
        field_data = input('Phone or email: ')

        field = contact.find(field_data)
        if field is None:
            print('Not found.')
            return

        contact.delete(field)

        print()
        print(contact)
        phonebook.history.record(contact)


    def delete_contact(self, phonebook, contact, **kwargs):
        confirm = input(
            'Are you sure you want to delete "{}"? (y/n): '.format(contact.name))
        if confirm in ['y', 'Y']:
            phonebook.delete(contact)
            print('Contact "{}" deleted.'.format(contact.name))
            self.back()


    def show_history(self, phonebook, contact, **kwargs):
        print('\nHistory')
        try:
            items = phonebook.history.read(contact)
        except FileNotFoundError:
            print('Could not find history.')

        for item in items:
            self._print_item(item)


    @staticmethod
    def _print_item(item):
        print('\n{}\n{}'.format(item['date'],
                                item['name']))
        for data, description in item['fields'].items():
            if description is None:
                print(data)
            else:
                print(data, description)


    @staticmethod
    def quit(phonebook, **kwargs):
        phonebook.save()
        print('Good Bye!')
        raise SystemExit('Exit')


    @staticmethod
    def back(*args, **kwargs):
        raise SystemExit('Back')


    def loop(self):
        self.main_menu.loop(self.phonebook)


def main():
    persistence = PicklePersistence()
    history = JSONHistory()
    phonebook = PhoneBook(persistence=persistence, history=history)
    # populate(phonebook)
    PhoneBookMenu(phonebook).loop()


if __name__ == '__main__':
    main()
