import collections


class Option():
    def __init__(self, title, action, submenu=None):
        self.title = title
        self.action = action
        self.submenu = submenu


class Menu(collections.OrderedDict):

    def show(self):
        print()
        print(self['title'])
        for key, option in self.items():
            if key == 'title':
                continue
            print('{}. {}'.format(key, option.title))

    def loop(self, *domain_objects):
        while True:
            self.show()
            choice = self.input()
            try:
                self.handle_input(choice, *domain_objects)
            except SystemExit:
                break

    def input(self):
        while True:
            choice = input('>>> ')
            if choice in ['q', 'Q']:
                choice = '0'
                
            if choice not in self:
                print('Choice not found.')
            else:
                return choice
            
    def handle_input(self, choice, *domain_objects):
        option = self[choice]
        action = option.action
        submenu = option.submenu
        action(*domain_objects, submenu=submenu)
