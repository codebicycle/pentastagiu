from abc import ABCMeta, abstractmethod
import re

class Field(metaclass=ABCMeta):
    def __init__(self, data, description=None):
        self.validate(data)

        if description and description.strip() == '':
            description = None

        self.data = data
        self.description = description

    @abstractmethod
    def validate(self, *args, **kwargs):
        pass


class Phone(Field):
    PHONE = re.compile(r'^\+?[\d\(\) ]+$')

    def __eq__(self, other):
        return self.data == other.data

    def __repr__(self):
        if not self.description:
            return '{}'.format(self.data)
        return '{} {}'.format(self.data, self.description)

    @classmethod
    def _is_phone_valid(cls, phone_number):
        return bool(cls.PHONE.match(phone_number))

    @classmethod
    def validate(cls, phone_number):
        if not cls._is_phone_valid(phone_number):
            raise ValueError(
                'Invalid phone number. '
                'Only digits, spaces, "+" and parenthesis accepted.')


class Name(Field):
    NAME = re.compile(r'^[a-zA-Z ]+$')

    def __eq__(self, other):
        return self.data.lower() == other.data.lower()

    def __repr__(self):
        return str(self.data)

    @classmethod
    def _is_name_valid(cls, name):
        return bool(cls.NAME.match(name))

    @classmethod
    def validate(cls, name):
        if not cls._is_name_valid(name):
            raise ValueError('Invalid name. '
                             'Only letters and spaces are permitted.')


class Email(Field):
    EMAIL = re.compile(r'^\S+@\S+\.\S+$')

    def __eq__(self, other):
        return self.data.lower() == other.data.lower()

    def __repr__(self):
        if not self.description:
            return '{}'.format(self.data)
        return '{} {}'.format(self.data, self.description)

    @classmethod
    def _is_email_valid(cls, email):
        return bool(cls.EMAIL.match(email))

    @classmethod
    def validate(cls, email):
        if not cls._is_email_valid(email):
            raise ValueError('Invalid email.')
