import datetime
import json
import os

from field import Name

class Contact():
    def __init__(self, name):
        self.id = None
        self.name = Name(name)
        self.fields = []

    def __eq__(self, other):
        return self.name == other.name

    def add(self, field):
        if field in self.fields:
            raise ValueError('Duplicate Error. Field already exist.')
        self.fields.append(field)
        return self

    def delete(self, field):
        self.fields.remove(field)

    def find(self, field_data):
        found = None
        for field in self.fields:
            if field.data == field_data:
                found = field
        return found

    def update(self, field, field_data=None, description=None):
        if field_data:
            field.validate(field_data)
            field.data = field_data

        if description:
            field.description = description

    def __repr__(self):
        fields_string = '\n'.join(map(str, self.fields))
        return '{}\n{}'.format(self.name, fields_string)

    def __str__(self):
        return '\t' + repr(self)

    def to_json(self):
        item = {}
        item['date'] = '{:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now())
        item['name'] = self.name.data
        item['fields'] = {field.data: field.description
                           for field in self.fields}
        json_str = json.dumps(item)
        return json_str
