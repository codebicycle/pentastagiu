import logging
import pickle
import os

from contact import Contact
from field import Phone

logger = logging.getLogger(__name__)


class PhoneBook():
    def __init__(self, persistence, history, name='phonebook'):
        self.name = name
        self.persistence = persistence(self)
        self.history = history(self)

        existing_phonebook = self.persistence.get_or_create()
        if existing_phonebook is not None:
            logger.debug('Existing phonebook found.')
            self.__dict__.update(existing_phonebook.__dict__)
            return None

        logger.debug('No phonebook found, creating a new one.')
        self._current_id = 0
        self._contacts = []
        self._add_defaults()


    def __repr__(self):
        return '\n'.join(map(str, self._contacts))


    def _add_defaults(self):
        self.add(Contact('Urgenta').add(Phone('112')))
        self.add(Contact('Serviciu Client').add(Phone('433')))
        self.add(Contact('Credit').add(Phone('333')))


    def add(self, contact):
        if contact in self._contacts:
            raise ValueError('Duplicate Error. Contact "{}" already exist.'
                             .format(contact.name))
        self._current_id += 1
        contact.id = self._current_id
        self._contacts.append(contact)
        self.save()
        self.history.record(contact)


    def find(self, name):
        found = None
        for contact in self._contacts:
            if contact.name.data.lower() == name.lower():
                found = contact
        return found


    def delete(self, contact):
        self._contacts.remove(contact)
        self.save()


    def paginate(self, per_page=5):
        for i in range(0, len(self._contacts), per_page):
             yield self._contacts[i: i + per_page]


    def save(self):
        self.persistence.save()
