import re

PHONE = re.compile(r'^\+?[\d\(\) ]+$')


class Phone():
    def __init__(self, number, type_):
        self.validate_phone_number(number)
        self.number = number.strip()
        self.type = type_

    def __eq__(self, other):
        return self.number == other.number

    def __repr__(self):
        if not self.type:
            return '{}'.format(self.number)
        return '{} {}'.format(self.number, self.type)

    @staticmethod
    def _is_phone_valid(phone):
        return bool(PHONE.match(phone))

    @classmethod
    def validate_phone_number(cls, phone_number):
        if not cls._is_phone_valid(phone_number):
            raise ValueError(
                'Invalid phone number. '
                'Only digits, spaces, "+" and parenthesis accepted.')
