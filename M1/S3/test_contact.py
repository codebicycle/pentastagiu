import pytest

from contact import Contact, Phone


def test_contact_init():
    contact = Contact('B', '123', 'home')
    assert 'B' == contact.name
    first_phone = contact.phones[0]
    assert '123' == first_phone.number
    assert 'home' == first_phone.type

    contact = Contact('C')
    assert 'C' == contact.name
    assert 0 == len(contact.phones)

    with pytest.raises(ValueError):
        Contact('2')

    with pytest.raises(ValueError):
        Contact('A', 'no digits')

def test_contact_add():
    contact = Contact('A')
    phone1 = Phone('111', 'mobile')
    phone2 = Phone('222', 'landline')

    contact.add(phone1)
    assert 1 == len(contact.phones)
    assert '111' == contact.phones[0].number
    assert 'mobile' == contact.phones[0].type

    contact.add(phone2)
    assert 2 == len(contact.phones)
    assert '222' == contact.phones[1].number
    assert 'landline' == contact.phones[1].type

def test_contact_find(contact):
    phone = contact.find('222')
    assert '222' == phone.number
    assert 'landline' == phone.type

    no_phone = contact.find('000')
    assert no_phone is None

def test_contact_delete(contact):
    phone = contact.find('222')
    contact.delete(phone)
    assert 1 == len(contact.phones)

    phone = contact.find('222')
    assert phone is None

def test_contact_update(contact):
    phone = contact.find('222')
    contact.update(phone, '333')
    assert '333' in [p.number for p in contact.phones]

    phone = contact.find('333')
    contact.update(phone, phone_type='work')
    expected = Phone('333', 'work')
    assert expected in contact.phones

@pytest.fixture   
def contact():
    john = Contact('John Doe')
    phone1 = Phone('111', 'mobile')
    phone2 = Phone('222', 'landline')

    john.add(phone1)
    john.add(phone2)
    return john