
from phonebook import PhoneBook

def test_phonebook_init():
    phonebook = PhoneBook()
    assert 3 == len(phonebook.contacts)
