
from contact import Contact

class PhoneBook(dict):
    def __init__(self):
        self.contacts = [
            Contact('Urgenta', '112'),
            Contact('Serviciu Client', '433'),
            Contact('Credit', '333'),
        ]

    def add(self, contact):
        if contact in self.contacts:
            raise ValueError('Duplicate Error. Contact already exist.')
        self.contacts.append(contact)

    def find(self, name):
        found = None
        for contact in self.contacts:
            if contact.name.lower() == name.lower():
                found = contact
        return found

    def delete(self, contact):
        self.contacts.remove(contact)

    def update(self, contact, name=None):
        if name:
            Contact.validate_name(name)
        contact.name = name

    def paginate(self, per_page=5):
        for i in range(0, len(self.contacts), per_page):
             yield self.contacts[i: i + per_page]

    def __repr__(self):
        return '\n'.join(map(str, self.contacts))
