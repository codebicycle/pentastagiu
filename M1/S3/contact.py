import collections
import re

from phone import Phone

NAME = re.compile(r'^[a-zA-Z ]+$')


class Contact():
    def __init__(self, name, phone_number=None, phone_type=None):
        self.validate_name(name)

        self.name = name.strip()
        self.phones = []

        if phone_number:
            phone = Phone(phone_number, phone_type)
            self.phones.append(phone)

    def __eq__(self, other):
        return self.name.lower() == other.name.lower()

    def add(self, phone):
        if phone in self.phones:
            raise ValueError('Duplicate Error. Phone already exist.')
        self.phones.append(phone)

    def delete(self, phone):
        self.phones.remove(phone)

    def find(self, phone_number):
        found = None
        for phone in self.phones:
            if phone.number == phone_number:
                found = phone
        return found

    def update(self, phone, phone_number=None, phone_type=None):
        if phone_number:
            Phone.validate_phone_number(phone_number)
            phone.number = phone_number

        if phone_type:
            phone.type = phone_type

    def __repr__(self):
        phones_string = ', '.join(map(str, self.phones))
        return '{}: {}'.format(self.name, phones_string)

    def __str__(self):
        return '\t' + repr(self)

    @staticmethod
    def _is_name_valid(name):
        return bool(NAME.match(name))

    @classmethod
    def validate_name(cls, name):
        if not cls._is_name_valid(name):
            raise ValueError('Invalid name. '
                             'Only letters and spaces are permitted.')
