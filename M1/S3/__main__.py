import csv
import sys

from contact import Contact
from phonebook import PhoneBook
from menu import Menu, Option
from phone import Phone


def populate(phonebook):
    with open('contacts.csv') as f:
        reader = csv.DictReader(f)
        for row in reader:
            current = Contact(row['name'])
            current.add(Phone(row['phone1'], 'mobile'))
            current.add(Phone(row['phone2'], 'home'))
            current.add(Phone(row['phone3'], 'work'))
            phonebook.add(current)


class PhoneBookMenu():
    def __init__(self, phonebook):
        self.phonebook = phonebook

        self.contact_menu =  Menu('Contact Options', [
            ('1', Option('Edit Contact', self.edit)),
            ('2', Option('Add Phone Number', self.add_phone)),
            ('3', Option('Remove Phone Number', self.remove_phone)),
            ('4', Option('Delete Contact', self.delete_contact)),
            ('0', Option('Back', self.back)),
        ])

        self.main_menu = Menu('Phone Book', [
            ('1', Option('Show Contacts', self.show_contacts)),
            ('2', Option('Search', self.search, self.contact_menu)),
            ('3', Option('Add Contact', self.add_contact)),
            ('0', Option('Exit', self.quit)),
        ])

    @staticmethod
    def search(phonebook, submenu):
        print('\nSearch Contacts')
        name = input('Name: ')

        contact = phonebook.find(name)
        if contact is None:
            print('Not found.')
            return

        print()
        print(contact)

        submenu.loop(phonebook, contact)

    @staticmethod
    def show_contacts(phonebook, **kwargs):
        for page in phonebook.paginate():
            print('\n'.join(map(str, page)))
            choice = input(':')
            if choice in set('qQ0'):
                break

    @staticmethod
    def add_contact(phonebook, **kwargs):
        print('\nAdd Contact')
        name = input('Name: ')
        phone_number = input('Phone number: ')
        phone_type = input('Phone type: ')

        try:
            contact = Contact(name, phone_number, phone_type)
        except ValueError as e:
            print(e)
            return

        try:
            phonebook.add(contact)
        except ValueError as e:
            print(e)
            return

        print()
        print(contact)

    @staticmethod
    def edit(phonebook, contact, **kwargs):
        print('\nEdit Contact (Enter to keep the old value)')
        name = input('Name ({}): '.format(contact.name))
        if name == '':
            name = contact.name

        try:
            phonebook.update(contact, name)
        except ValueError as e:
            print(e)
            return
        finally:
            print(contact)

        for phone in contact.phones:
            phone_number = input('Phone number ({}): '.format(phone.number))
            phone_type = input('Phone type ({}): '.format(phone.type))

            if phone_number == '':
                phone_number = phone.number
            if phone_type == '':
                phone_type = phone.type

            try:
                contact.update(phone, phone_number, phone_type)
            except ValueError as e:
                print(e)
                return
            finally:
                print(contact)

    @staticmethod
    def add_phone(phonebook, contact, **kwargs):
        print('\nAdd Phone Number')
        phone_number = input('Phone number: ')
        phone_type = input('Phone type: ')

        if phone_number:
            try:
                phone = Phone(phone_number, phone_type)
            except ValueError as e:
                print(e)
                return

            try:
                contact.add(phone)
            except ValueError as e:
                print(e)
                return

            print()
            print(contact)

    @staticmethod
    def remove_phone(phonebook, contact, **kwargs):
        print('\nRemove Phone Number')
        phone_number = input('Phone number: ')

        phone = contact.find(phone_number)
        if phone is None:
            print('Not found.')
            return

        contact.delete(phone)

        print()
        print(contact)

    def delete_contact(self, phonebook, contact, **kwargs):
        confirm = input(
            'Are you sure you want to delete "{}"? (y/n): '.format(contact.name))
        if confirm in ['y', 'Y']:
            phonebook.delete(contact)
            print('Contact "{}" deleted.'.format(contact.name))
            self.back()

    @staticmethod
    def quit(*args, **kwargs):
        print('Good Bye!')
        raise SystemExit('Exit')

    @staticmethod
    def back(*args, **kwargs):
        raise SystemExit('Back')

    def loop(self):
        self.main_menu.loop(self.phonebook)


def main():
    phonebook = PhoneBook()
    populate(phonebook)
    PhoneBookMenu(phonebook).loop()


if __name__ == '__main__':
    main()
