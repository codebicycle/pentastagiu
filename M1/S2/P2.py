
def p2(*args):
    """Min and max values for the given numbers

    :params args: numbers.
    :return: tuple (min, max)

    >>> p2(1)
    (1, 1)
    >>> p2(1, 2)
    (1, 2)
    >>> p2(1, 2, 10)
    (1, 10)
    >>> p2()
    Traceback (most recent call last):
    ...
    ValueError: Expected at least one parameter but none were given
    """

    if not args:
        raise ValueError('Expected at least one parameter but none were given')

    return min(args), max(args)


if __name__ == "__main__":
    import doctest
    doctest.testmod()
