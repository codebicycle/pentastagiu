
from P12 import p12


def test_p12_int():
    cmp1 = p12(0)

    result = cmp1(2, 3)
    expected = -1
    assert expected == result

    result = cmp1(3, 2)
    expected = 1
    assert expected == result

    result = cmp1(3, 3)
    expected = 0
    assert expected == result


def test_p12_str():
    cmp1 = p12('')

    result = cmp1('ana are', 'mere')
    expected = 1
    assert expected == result

    result = cmp1('mere', 'ana are')
    expected = -1
    assert expected == result

    result = cmp1('mere', 'pere')
    expected = 0
    assert expected == result
