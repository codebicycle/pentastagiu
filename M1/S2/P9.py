
def p9(sequence, func, start=0, pas=1):
    """Selective map

    Return a new list of the same length as the sequence given as parameter.

    >>> p9('abecedar', str.upper, 0, 2)
    ['A', 'b', 'E', 'c', 'E', 'd', 'A', 'r']

    >>> p9('abecedar', str.upper, 4, 1)
    ['a', 'b', 'e', 'c', 'E', 'D', 'A', 'R']
    """

    accumulator = []
    target_indexes = range(start, len(sequence), pas)

    for index, item in enumerate(sequence):
        processed_item = func(item) if index in target_indexes else item
        accumulator.append(processed_item)

    return accumulator


def p9_2(sequence, func, start=0, pas=1):
    """Selective map

    Only the affected elements mapped values are returned in a list.

    >>> p9_2('abecedar', str.upper, 0, 2)
    ['A', 'E', 'E', 'A']

    >>> p9_2('abecedar', str.upper, 4, 1)
    ['E', 'D', 'A', 'R']
    """

    return [func(item)
            for index, item in enumerate(sequence)
            if index in range(start, len(sequence), pas)]
