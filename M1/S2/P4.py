
def p4(n):
    """Print star triangle, recursive

    :param n: integer
    :return: None

    >>> p4(0)
    >>> p4(1)
    *
    >>> p4(2)
    *
    * *
    >>> p4(3)
    *
    * *
    * * *
    >>> p4(6)
    *
    * *
    * * *
    * * * *
    * * * * *
    * * * * * *
    """

    if n <= 0:
        return

    line = '* ' * n
    line = line.rstrip()

    p4(n - 1)

    print(line)


if __name__ == "__main__":
    import doctest
    doctest.testmod()
