
def p7(func, iterable):
    """Map function over iterable"""

    if isinstance(iterable, dict):
        mapping = {func(key): value for key, value in iterable.items()}
        constructor = type(iterable)
        return constructor(mapping)

    generator = (func(item) for item in iterable)

    if isinstance(iterable, str):
        return ''.join(generator)
    else:
        constructor = type(iterable)
        return constructor(generator)
