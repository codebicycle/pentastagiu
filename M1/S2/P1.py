
def p1(*sides):
    """Polygon perimeter

    :params *sides: polygon sides lengths.

    >>> p1(1, 1, 1)
    3

    >>> p1(1, 1, 1, 2)
    5

    >>> p1()
    Traceback (most recent call last):
    ...
    ValueError: A polygon must have at least three sides

    >>> p1(0, 0, 0)
    Traceback (most recent call last):
    ...
    ValueError: Polygon side lengths must be greater than zero

    >>> p1(10, 1, 1)
    Traceback (most recent call last):
    ...
    ValueError: No valid polygon for the side lengths given

    """

    if len(sides) < 3:
        raise ValueError('A polygon must have at least three sides')

    if any(side <= 0 for side in sides):
        raise ValueError('Polygon side lengths must be greater than zero')

    if not is_valid_polygon(sides):
        raise ValueError('No valid polygon for the side lengths given')

    perimeter = sum(sides)
    return perimeter


def is_valid_polygon(sides):
    """Each side is less then the sum of the other sides

    :param sides: sequence of polygon side lengths.
    """

    sum_sides = sum(sides)
    for side in sides:
        sum_other_sides = sum_sides - side
        if side >= sum_other_sides:
            return False
    return True


if __name__ == "__main__":
    import doctest
    doctest.testmod()
