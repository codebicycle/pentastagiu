
import math


def prime_sieve(n):
    """Sieve of Eratosthenes

    Compute primes up to n.

    >>> prime_sieve(30)
    [2, 3, 5, 7, 11, 13, 17, 19, 23, 29]
    >>> prime_sieve(71)
    [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71]
    """

    table = [True] * (n+1)
    table[0] = False
    table[1] = False
    limit = math.ceil(math.sqrt(n))

    for i in range(2, limit + 1):
        if table[i] is True:
            for j in range(i*i, n+1, i):
                table[j] = False

    return [index
            for index, is_prime in enumerate(table)
            if is_prime is True]


if __name__ == "__main__":
    import doctest
    doctest.testmod()
