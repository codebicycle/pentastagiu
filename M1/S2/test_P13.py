
import collections
import textwrap

from P13 import calculate_deviations, print_below_target


def test_weight_monitor_components(capsys):
    measurements = collections.OrderedDict()
    measurements['2017-03-01'] = 74.5
    measurements['2017-03-02'] = 74.6
    measurements['2017-03-07'] = 75
    measurements['2017-03-08'] = 74.9

    target_weight = 75
    deviations = calculate_deviations(target_weight, measurements)

    print_below_target(measurements, deviations)
    out, err = capsys.readouterr()

    expected = """
    Weight below target:
    date: 2017-03-01, weight difference: -0.50
    date: 2017-03-02, weight difference: -0.40
    date: 2017-03-08, weight difference: -0.10
    """
    expected = textwrap.dedent(expected)

    assert expected == out
