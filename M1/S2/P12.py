
def p12(type_):
    """Comparator function

    By value for integers, by length for strings
    """

    if isinstance(type_, str):
        key = len
    elif isinstance(type_, int):
        key = lambda x: x

    def comparator(a, b):
        if key(a) == key(b):
            return 0
        elif key(a) > key(b):
            return 1
        else:
            return -1

    return comparator
