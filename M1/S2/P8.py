
def make_comparator(key):
    def comparator(a, b):
            if key(a) == key(b):
                return 0
            elif key(a) > key(b):
                return 1
            else:
                return -1

    return comparator


def p8(sequence, cmp):
    """Sort sequence with comparator function

    :param sequence: list
    :param cmp: function
    :return: new list ordered according to the comparator function
    """

    clone = sequence.copy()

    while True:
        swap = False
        for i in range(1, len(clone)):
            prev = clone[i - 1]
            current = clone[i]

            if cmp(prev, current) == 1:
                clone[i - 1], clone[i] = current, prev
                swap = True

        if not swap:
            return clone
