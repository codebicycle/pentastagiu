
def tower_of_hanoi(disks):
    source = list(range(disks, 0, -1))
    target = []
    aux = []

    def move(disks, from_, to, intermediary):
        if disks > 0:
            move(disks - 1, from_, intermediary, to)

            if from_:
                to.append(from_.pop())
            status()

            move(disks - 1, intermediary, to, from_)

    def status():
        print('{:6}: {}'.format('source', source))
        print('{:6}: {}'.format('aux', aux))
        print('{:6}: {}'.format('target', target))
        print()

    status()
    move(disks, source, target, aux)


def main():
    tower_of_hanoi(5)


if __name__ == '__main__':
    main()
