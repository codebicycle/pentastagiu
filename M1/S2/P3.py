
def p3(n):
    """Calculate sum of first n numbers using recursion

    :param n: integer
    :return: integer

    >>> p3(-1)
    0
    >>> p3(0)
    0
    >>> p3(1)
    1
    >>> p3(2)
    3
    >>> p3(3)
    6
    >>> p3(4)
    10
    """

    if n <= 0:
        return 0
    return n + p3(n - 1)


if __name__ == "__main__":
    import doctest
    doctest.testmod()
