import collections


def lookup(name, contacts):
    string_ = '{:20} {}'.format(name, contacts[name])
    print(string_)


def main():
    contacts = collections.defaultdict(list)

    contacts['emergency'].append('112')
    contacts['emergency'].append('911')
    contacts['taxi'].append('22 22 22')

    lookup('emergency', contacts)
    lookup('taxi', contacts)
    lookup('imaginary friend', contacts)


if __name__ == '__main__':
    main()
