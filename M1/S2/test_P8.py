
from P8 import p8, make_comparator


def test_p8_identity():
    cmp = make_comparator(lambda x: x)
    sequence = [5, 3, 5, 7, 3, 2, 9]
    
    result = p8(sequence, cmp=cmp)
    expected = [2, 3, 3, 5, 5, 7, 9]
    assert expected == result


def test_p8_len():
    cmp = make_comparator(len)
    sequence = 'mnop xyz ab o'.split()
    
    result = p8(sequence, cmp=cmp)
    expected = 'o ab xyz mnop'.split()
    assert expected == result
