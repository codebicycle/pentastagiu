import collections

import pytest

from P7 import p7


STRING = 'abecedar'

objects = [
    STRING,
    list(STRING),
    tuple(STRING),
    set(STRING),
    {'a': 1, 'b': 0, 'c': 2},
    collections.deque(STRING),
    collections.Counter(STRING),
]


@pytest.mark.parametrize('iterable', objects)
def test_p7_various_types(iterable):
    func = str.upper
    cls = type(iterable)

    expected = map(func, iterable)
    result = p7(func, iterable)

    assert cls == type(result)
    assert sorted(expected) == sorted(result)
