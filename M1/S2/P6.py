
def p6(word, L=None):
    """Swapcase to list, recursive

    :param word: string
    :param L: list
    :return: list L appended with case swapped word letters

    >>> p6('pOSTGREsql')
    ['P', 'o', 's', 't', 'g', 'r', 'e', 'S', 'Q', 'L']

    >>> p6('SwapCase', L=[])
    ['s', 'W', 'A', 'P', 'c', 'A', 'S', 'E']

    >>> p6('ABeCedar', L=[])
    ['a', 'b', 'E', 'c', 'E', 'D', 'A', 'R']

    >>> letters = list('Hello ')
    >>> p6('wORLD!', L=letters)
    ['H', 'e', 'l', 'l', 'o', ' ', 'W', 'o', 'r', 'l', 'd', '!']
    """

    if L is None:
        L = []

    if not word:
        return L

    head = word[0]
    tail = word[1:]

    L.append(head.swapcase())
    p6(tail, L=L)

    return L


if __name__ == "__main__":
    import doctest
    doctest.testmod()
