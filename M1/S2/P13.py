
import collections


def weight_monitor(target_weight, days=7):
    measurements = input_measurements(days)
    deviations = calculate_deviations(target_weight, measurements)
    print_below_target(measurements, deviations)


def input_measurements(days):
    measurements = collections.OrderedDict()

    for day in range(1, days + 1):
        date = input('\nDay {}\nenter date: '.format(day)).strip()
        weight = float(input('enter weight: '))

        measurements[date] = weight

    return measurements


def calculate_deviations(target_weight, measurements):
    deviations = collections.Counter()
    for date, weight in measurements.items():
        deviations[date] = weight - target_weight

    return deviations


def print_below_target(measurements, deviations):
    print('\nWeight below target:')
    for date in measurements:
        if deviations[date] < 0:
            print('date: {}, weight difference: {:0.2f}'
                  .format(date, deviations[date]))


def main():
    weight_monitor(75, days=4)


if __name__ == '__main__':
    main()
