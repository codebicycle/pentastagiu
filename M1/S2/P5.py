import collections


def p5(word):
    """Is palindrome

    :param word: string
    :return: boolean: True if word is palindrome, False otherwise

    >>> p5('radar')
    True
    >>> p5('noon')
    True
    >>> p5('hello')
    False
    >>> p5('1984')
    False
    >>> p5('a')
    True
    >>> p5('')
    True
    """

    word_deque = collections.deque(word.lower())

    while len(word_deque) > 2:
        head = word_deque.popleft()
        tail = word_deque.pop()

        if head != tail:
            return False
    return True


if __name__ == "__main__":
    import doctest
    doctest.testmod()
