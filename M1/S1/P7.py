def sum_of_n_loop(n):
    """Sum of first n numbers

    :param n: integer
    :return: integer
    """

    if n < 0:
        raise ValueError('Expecting a positive value for n, {} given.'
                         .format(n))
    total = 0
    for i in range(n + 1):
        total += i
    return total


def main():
    n = int(input('Enter n: '))
    total = sum_of_n_loop(n)
    print('S = {}'.format(total))


if __name__ == '__main__':
    main()
