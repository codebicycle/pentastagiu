import sys


def main():
    if len(sys.argv) < 2:
        print('Usage: python <script.py> WORD_1 WORD_2 WORD_3 ... WORD_N')
        sys.exit()

    words = sys.argv[1:]
    ordered_words = sorted(words,
                           key=lambda word: (len(word), word))
    print(' '.join(ordered_words))


if __name__ == '__main__':
    main()
