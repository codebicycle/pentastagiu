def is_palindrome(string_):
    string_ = string_.lower()
    return string_ == string_[::-1]


def main():
    string_ = input('Enter string: ')
    output = 'true' if is_palindrome(string_) else 'false'
    print(output)


if __name__ == '__main__':
    main()
