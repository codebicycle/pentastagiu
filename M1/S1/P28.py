import re
import sys
import collections

VOWELS = re.compile(r'[aeiou]', re.IGNORECASE)


def vowels_count(word):
    vowels = VOWELS.findall(word)
    return len(vowels)


def main():
    if len(sys.argv) < 2:
        print('Usage: python <script.py> WORD_1 WORD_2 WORD_3 ... WORD_N')
        sys.exit()

    words = sys.argv[1:]
    for word in words:
        num_vowels = vowels_count(word)
        frequency = collections.Counter(word)
        print('{} {} {}'.format(word, num_vowels, dict(frequency)))


if __name__ == '__main__':
    main()
