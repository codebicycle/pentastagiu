import math

from utils import is_prime


def main():
    number = int(input('Enter number: '))
    output = 'true' if is_prime(number) else 'false'
    print(output)


if __name__ == '__main__':
    main()
