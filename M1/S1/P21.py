import sys

from utils import to_ints


def main():
    if len(sys.argv) < 2:
        print('Usage: python <script.py> NUMBER_1 NUMBER_2 NUMBER_3 ... NUMBER_N')
        sys.exit()

    numbers_raw = sys.argv[1:]
    numbers = to_ints(numbers_raw)
    print(sorted(numbers))


if __name__ == '__main__':
    main()
