import math
import itertools

from utils import is_prime


def first_primes(n):
    if n < 1:
        return []
    primes = [2]
    candidates = itertools.count(start=3, step=2)
    while len(primes) < n:
        candidate = next(candidates)
        if is_prime(candidate):
            primes.append(candidate)
    return primes


def main():
    n = int(input('Enter n: '))
    primes = first_primes(n)
    primes_str = map(str, primes)
    print(', '.join(primes_str))


if __name__ == '__main__':
    main()
