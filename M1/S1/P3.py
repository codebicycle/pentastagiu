def sum_digits(number):
    number = abs(number)
    total = 0
    while number != 0:
        total += number % 10
        number //= 10

    return total


def main():
    number = int(input('Enter number: '))
    total = sum_digits(number)
    print('S = {}'.format(total))


if __name__ == '__main__':
    main()
