import string
import sys


def build_mapping():
    alphabet = string.ascii_uppercase + string.ascii_lowercase
    values = range(1, len(alphabet) + 1)
    letter_values = dict(zip(alphabet, values))
    return letter_values


LETTER_VALUES = build_mapping()


def sum_letters(word):
    chars = list(word)
    total = 0
    for char in chars:
        if char in LETTER_VALUES:
            total += LETTER_VALUES[char]
    return total


def main():
    if len(sys.argv) < 3:
        print('Usage: python <script.py> "SENTENCE" "SEPARATOR"')
        sys.exit()

    sentence = sys.argv[1]
    separator = sys.argv[2]

    words = sentence.split(separator)
    max_sum_letters = max(words, key=sum_letters)
    min_sum_letters = min(words, key=sum_letters)

    print(max_sum_letters)


if __name__ == '__main__':
    main()
