import sys

from utils import to_ints


def number_to_words():
    """Mapping from numbers to words. Used to read time. Max is 59.

    :return: dictionary
    e.g. {...
          30: 'treizeci',
          31: 'treizeci si unu',
          32: 'treizeci si doi',
          33: 'treizeci si trei',
          ...}
    """
    words_str = ('zero unu doi trei patru cinci sase sapte opt noua zece '
                 'unsprezece doisprezece treisprezece paisprezece cincisprezece '
                 'saisprezece saptesprezece optsprezece nouasprezece '
                 'douazeci treizeci patruzeci cincizeci')
    digits_str = '0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 30 40 50'

    words = words_str.split()
    digits = to_ints(digits_str.split())

    numerals = dict(zip(digits, words))

    for i in range(21, 60):
        if numerals.get(i) is None:
            units = i % 10
            tens = i - units
            composed = '{} si {}'.format(numerals[tens], numerals[units])
            numerals[i] = composed

    return numerals


def time_words(time_str):
    time_parts = time_str.split(':')
    hours, minutes = to_ints(time_parts)

    if hours < 0 or hours > 23:
        raise ValueError('Invalid hour {}'.format(hours))
    if minutes < 0 or minutes > 59:
        raise ValueError('Invalid minute {}'.format(minutes))

    numerals = number_to_words()
    hours_words = numerals[hours]
    minutes_words = numerals[minutes]

    if minutes == 0:
        time_in_words = hours_words
    else:
        time_in_words = '{} si {}'.format(hours_words, minutes_words)

    return time_in_words


def main():
    if len(sys.argv) < 2:
        print('Usage: python <script.py> "04:32"')
        sys.exit()

    time_str = sys.argv[1]
    time_in_words = time_words(time_str)
    print(time_in_words)


if __name__ == '__main__':
    main()
