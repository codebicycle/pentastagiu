from utils import is_valid_polygon, multiline_input, to_floats


def polygon_perimeter(sides):
    """
    :param sides: sequence of polygon side lengths.
    """

    if len(sides) < 3:
        raise ValueError('A polygon must have at least three sides')

    if any(side < 0 for side in sides):
        raise ValueError('Polygon side lengths must be positive')

    if not is_valid_polygon(sides):
        raise ValueError('No valid polygon for the side lengths given')

    return sum(sides)


def main():
    sides_raw = multiline_input('Enter polygon side lengths, on separate lines')
    side_lengths = to_floats(sides_raw)
    perimeter = polygon_perimeter(side_lengths)
    print('P = {}'.format(perimeter))

if __name__ == '__main__':
    main()
