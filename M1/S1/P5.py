from utils import is_valid_polygon


INVALID = 'invalid'
EQUILATERAL = 'echilateral'
ISOSCELES = 'isoscel'
SCALENE = 'oarecare'


def triangle_type(a, b, c):
    """
    :params a, b, c: triangle side lengths
    """

    if not is_valid_polygon((a, b, c)):
        return INVALID
    elif a == b == c:
        return EQUILATERAL
    elif a == b or a == c or b == c:
        return ISOSCELES
    else:
        return SCALENE


def main():
    first_side = int(input('Enter triangle first side\'s length: '))
    second_side = int(input('Enter triangle second side\'s length: '))
    third_side = int(input('Enter triangle third side\'s length: '))

    kind = triangle_type(first_side, second_side, third_side)
    print(kind)


if __name__ == '__main__':
    main()
