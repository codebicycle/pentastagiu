import operator
import sys
import functools

from utils import to_ints


def main():
    if len(sys.argv) < 2:
        print('Usage: python <script.py> NUMBER_1 NUMBER_2 NUMBER_3 ... NUMBER_N')
        sys.exit()

    numbers_raw = sys.argv[1:]
    numbers = to_ints(numbers_raw)
    product = functools.reduce(operator.mul, numbers)
    print('P = {}'.format(product))


if __name__ == '__main__':
    main()
