import pytest

from P27 import time_words


def test_time_words_general_values():
    time_str = '04:32'
    expected = 'patru si treizeci si doi'
    assert expected == time_words(time_str)

    time_str = '13:14'
    expected = 'treisprezece si paisprezece'
    assert expected == time_words(time_str)

    time_str = '21:30'
    expected = 'douazeci si unu si treizeci'
    assert expected == time_words(time_str)

    time_str = '8:45'
    expected = 'opt si patruzeci si cinci'
    assert expected == time_words(time_str)


def test_time_words_zero_hours():
    time_str = '00:32'
    expected = 'zero si treizeci si doi'
    assert expected == time_words(time_str)


def test_time_words_zero_minutes():
    time_str = '12:00'
    expected = 'doisprezece'
    assert expected == time_words(time_str)


def test_time_words_zero_hours_zero_minutes():
    time_str = '00:00'
    expected = 'zero'
    assert expected == time_words(time_str)


def test_time_words_max_value():
    time_str = '23:59'
    expected = 'douazeci si trei si cincizeci si noua'
    assert expected == time_words(time_str)


def test_time_words_raises_on_hours_overflow():
    with pytest.raises(ValueError):
        time_str = '24:00'
        time_words(time_str)


def test_time_words_raises_on_minutes_overflow():
    with pytest.raises(ValueError):
        time_str = '12:60'
        time_words(time_str)
