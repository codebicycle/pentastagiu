DICTIONARY = {
    'apple': 'mar',
    'car': 'masina',
    'bike': 'bicicleta',
    'beer': 'bere',
    'book': 'carte',
    'dog': 'caine',
}


def main():
    search_term = input('Enter search word: ')

    if search_term in DICTIONARY:
        print('Traducerea este:', DICTIONARY[search_term])
    else:
        print('Nu exista in dictionar!')


if __name__ == '__main__':
    main()
