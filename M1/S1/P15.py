def main():
    number = int(input('Enter number: '))
    print('{} {} {}'.format(number, oct(number), hex(number)))


if __name__ == '__main__':
    main()
