import re

from utils import multiline_input, to_ints


def main():
    input_str = input('Enter numbers separated by space: ')
    numbers_str = re.split(r'\s+', input_str)
    numbers = to_ints(numbers_str)
    print('{} {}'.format(numbers, numbers[::-1]))


if __name__ == '__main__':
    main()
