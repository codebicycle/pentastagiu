from utils import multiline_input, to_ints


def main():
    inputs = multiline_input(prompt='Enter numbers on separate lines',
                             sentinel='0')
    numbers = to_ints(inputs)
    print('{} SUM = {}'.format(numbers, sum(numbers)))


if __name__ == '__main__':
    main()
