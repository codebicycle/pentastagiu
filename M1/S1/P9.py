def digits(number):
    number = abs(number)
    digits_reversed = []
    while number > 0:
        last_digit = number % 10
        digits_reversed.append(last_digit)
        number //= 10
    return reversed(digits_reversed)


def main():
    number = int(input('Enter number: '))
    max_digit = max(digits(number))
    print('CM = {}'.format(max_digit))


if __name__ == '__main__':
    main()
