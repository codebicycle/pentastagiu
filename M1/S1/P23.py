import re

from utils import multiline_input, to_ints


def partition(sequence, partition_length):
    accumulator = []
    for i in range(0, len(sequence) + 1, partition_length):
        accumulator.append(sequence[i:i + partition_length])
    return accumulator


def main():
    input_str = input('Enter list elements separated by space: ')
    numbers_str = re.split(r'\s+', input_str)
    numbers = to_ints(numbers_str)

    n = int(input('Enter n: '))

    sublists = partition(numbers, n)

    for sublist in sublists:
        sublist.sort()

    maximums = [sublist[-1] for sublist in sublists]
    print(' '.join(str(x) for x in maximums))


if __name__ == '__main__':
    main()
