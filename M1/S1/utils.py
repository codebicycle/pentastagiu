import math


def is_valid_polygon(sides):
    """Each side is less then the sum of the other sides

    :param sides: sequence of polygon side lengths.
    """

    sum_sides = sum(sides)
    for side in sides:
        sum_other_sides = sum_sides - side
        if side >= sum_other_sides:
            return False
    return True


def multiline_input(prompt='', sentinel='0'):
    """Accumulate input from multiple lines. Stop at sentinel value.
    Ignore whitespace-only lines.

    :param prompt: string
    :param sentinel: string
    :return: string list
    """

    inputs = []
    prompt_with_sentinel = '{prompt} (stop with {sentinel})\n'.format(
        prompt=prompt, sentinel=sentinel)
    current_input = input(prompt_with_sentinel)

    while current_input != sentinel:
        if current_input.strip() != '':
            inputs.append(current_input)
        current_input = input()

    return inputs


def to_floats(sequence):
    return [float(x) for x in sequence if x != '']


def to_ints(sequence):
    return [int(x) for x in sequence if x != '']


def is_prime(number):
    if number <= 1:
        return False
    if number == 2:
        return True
    if number % 2 == 0:
        return False

    limit = math.ceil(math.sqrt(number))
    for i in range(3, limit + 1, 2):
        if number % i == 0:
            return False
    return True


def gcd(a, b):
    """Greatest Common Divisor
    Better use math.gcd()
    """

    while b:
        a, b = b, a % b
    return a


def lcm(a, b):
    """Least Common Multiple"""
    return (a * b) // gcd(a, b)
