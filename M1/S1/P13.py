from utils import gcd


def main():
    a = int(input('Enter first number: '))
    b = int(input('Enter second number: '))
    result = gcd(a, b)

    print('CMMDC =', result)


if __name__ == '__main__':
    main()
