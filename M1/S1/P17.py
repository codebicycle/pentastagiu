import string


def swapcase_table():
    upper_lower = string.ascii_uppercase + string.ascii_lowercase
    lower_upper = string.ascii_lowercase + string.ascii_uppercase
    table = str.maketrans(upper_lower, lower_upper)
    return table


SWAPCASE_TABLE = swapcase_table()


def swapcase(string_):
    """Better use str.swapcase()"""
    return string_.translate(SWAPCASE_TABLE)


def main():
    string_ = input('Enter string: ')
    output = swapcase(string_)
    print(output)


if __name__ == '__main__':
    main()
