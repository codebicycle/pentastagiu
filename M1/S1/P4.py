from utils import multiline_input, to_floats


def main():
    numbers_raw = multiline_input(prompt='Enter numbers, separated by enter',
                                  sentinel='-1')
    numbers = to_floats(numbers_raw)
    minimum = min(numbers)
    maximum = max(numbers)
    print('min = {} -- max = {}'.format(minimum, maximum))


if __name__ == '__main__':
    main()
