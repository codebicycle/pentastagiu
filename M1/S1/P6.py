def sum_of_n(n):
    """Sum of first n numbers

    :param n: integer
    :return: integer
    """

    if n < 0:
        raise ValueError('Expecting a positive value for n, {} given.'
                         .format(n))
    return n * (n + 1) // 2


def main():
    n = int(input('Enter n: '))
    total = sum_of_n(n)
    print('S = {}'.format(total))


if __name__ == '__main__':
    main()
