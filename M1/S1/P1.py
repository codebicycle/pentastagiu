import math


def circle_area(radius):
    return math.pi * (radius ** 2)


def main():
    radius = float(input('Radius: '))
    if radius < 0:
        raise ValueError('Radius must be a positive value')

    area = circle_area(radius)
    print('A = {}'.format(area))


if __name__ == '__main__':
    main()
