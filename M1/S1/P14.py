from utils import lcm


def main():
    a = int(input('Enter first number: '))
    b = int(input('Enter second number: '))
    result = lcm(a, b)

    print('CMMMC =', result)


if __name__ == '__main__':
    main()
