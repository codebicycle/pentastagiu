import functools
import operator


def factorial(n):
    """Factorial
    Better use math.factorial()
    """

    if n < 0:
        raise ValueError('Expecting a positive value for n, {} given.'
                         .format(n))

    one_to_n = range(1, n + 1)
    return functools.reduce(operator.mul, one_to_n)


def main():
    n = int(input('Enter n: '))
    result = factorial(n)
    print('F = {}'.format(result))


if __name__ == '__main__':
    main()
