def triangle_lines(n):
    for i in range(1, n + 1):
        stars = ['*'] * i
        line = ' '.join(stars)
        yield line


def main():
    n = int(input('Enter n: '))
    for line in triangle_lines(n):
        print(line)


if __name__ == '__main__':
    main()
