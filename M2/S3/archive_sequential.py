import sys
from time import sleep
from utils import fast_archive, slow_archive

for filename in sys.argv[1:]:
    slow_archive(filename)
