from datetime import datetime
from functools import wraps
import time
import zipfile

def with_timing(func):
    @wraps(func)
    def wrapper(filename):
        start_time = time.perf_counter()
        print('{} Start {}'.format(datetime.now(), filename))

        func(filename)

        elapsed = time.perf_counter() - start_time
        print('{} Finished {} in {:.4} seconds.'.format(
            datetime.now(), filename, elapsed))
    return wrapper

def archive(filename):
    with zipfile.ZipFile(filename + '.zip', 'w') as myzip:
        myzip.write(filename)

@with_timing
def fast_archive(filename):
    archive(filename)
    
@with_timing
def slow_archive(filename):
    time.sleep(2)
    archive(filename)
