from multiprocessing import Pool
import sys
from utils import fast_archive, slow_archive


if __name__ == '__main__':
    if len(sys.argv) == 1:
        print('Usage: script.py FILE_ONE [FILE_TWO]')
        sys.exit()

    num_files = len(sys.argv) - 1
    with Pool(num_files) as pool:
        pool.map(slow_archive, sys.argv[1:])
