import sys
from threading import Thread
from utils import fast_archive, slow_archive


class Archival(Thread):
    def __init__(self, filename):
        self.filename = filename
        super().__init__()

    def run(self):
        slow_archive(self.filename)

if __name__ == '__main__':
    threads = [Archival(filename) for filename in sys.argv[1:]]

    for thread in threads:
        thread.start()

    print('Main done')

    for thread in threads:
        thread.join()
