from multiprocessing import Process
import sys
from utils import fast_archive, slow_archive


if __name__ == '__main__':
    processes = [Process(target=slow_archive, args=(filename,))
                for filename in sys.argv[1:]]

    for process in processes:
        process.start()

    print('Main done')

    for process in processes:
        process.join()
