import os


def _root_path():
    module_path = os.path.abspath(__file__)
    project_root = os.path.join(module_path, '..', '..')
    project_root = os.path.normpath(project_root)
    return project_root


ROOT_PATH = _root_path()
PERSISTENCE = os.path.join(ROOT_PATH, 'app.pickle')
