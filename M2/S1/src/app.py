import pickle

from config import PERSISTENCE
from menu import Menu, Option
from university import University, Faculty
from user import User
import utils

class App:
    def __init__(self):
        self._universities = []
        self._users = []

    def save(self, filepath=PERSISTENCE):
        with open(filepath, 'wb') as f:
            pickle.dump(self, f)

    def add_user(self, user):
        if user not in self._users:
            self._users.append(user)
            self.save()

    def add_university(self, university):
        if university not in self._universities:
            self._universities.append(university)
            self.save()

    def find_user(self, name):
        normalized_name = utils.normalize_name(name)
        for user in self._users:
            if user.name == normalized_name:
                return user

    def find_university(self, name):
        normalized_name = utils.normalize_name(name)
        for university in self._universities:
            if university.name == normalized_name:
                return university


class ConcreteMenu:
    def __init__(self, app):
        self.app = app

    def list_universities(self, **kwargs):
        print('List universities')
        if not self.app._universities:
            print('No universities to list.')

        for university in self.app._universities:
            print(university)
            for faculty in university._faculties:
                print('\t{}'.format(faculty))

    def simulate_admission(self, **kwargs):
        pass


class AdminMenu(ConcreteMenu):
    def __init__(self, app):
        super().__init__(app)

        self.admin_menu = Menu([
            ('title', 'Admin Menu'),
            ('1', Option('Add university', self.add_university)),
            ('2', Option('Add faculty', self.add_faculty)),
            ('9', Option('List universities', self.list_universities)),
            ('0', Option('Exit', self.quit)),
        ])

    def loop(self):
        self.admin_menu.loop()

    def add_university(self, **kwargs):
        print('Add university')
        name = input('Name: ')
        try:
            university = University(name)
        except ValueError as e:
            print(e)
            return

        self.app.add_university(university)

    def add_faculty(self, **kwargs):
        print('Add faculty')
        university_name = input('University name: ')
        university = self.app.find_university(university_name)
        if university is None:
            print('No University found with the given name.')
            return

        faculty_name = input('Faculty name: ')
        num_openings = int(input('Openings: '))
        try:
            faculty = Faculty(faculty_name, num_openings)
        except ValueError as e:
            print(e)
            return
        university.add_faculty(faculty)
        self.app.save()

    def quit(self, **kwargs):
        raise SystemExit('Exit')


class UserMenu(ConcreteMenu):
    def __init__(self, app):
        super().__init__(app)
        self.user = None

        self.user_menu = Menu([
            ('1', Option('Login/Register', self.login_register)),
            ('2', Option('List universities', self.list_universities)),
            ('3', Option('List applications', self.list_applications)),
            ('4', Option('Apply', self.apply)),
            ('5', Option('Renounce', self.renounce)),
            ('6', Option('Simulate admission', self.simulate_admission)),
            ('9', Option('Debug', self.debug)),
            ('0', Option('Exit', self.quit)),
        ])

    def loop(self):
        while self.user is None:
            self.login_register()
        self.user_menu.loop()

    def login_register(self, **kwargs):
        print('Login/Register')
        name = input('Name: ')

        user = self.app.find_user(name)
        if not user:
            try:
                user = self.register(name)
            except ValueError as e:
                print(e)
                return

        self.user = user
        self._update_menu_title()

    def _update_menu_title(self):
        self.user_menu['title'] = 'User: {}'.format(self.user.name)

    def list_applications(self, **kwargs):
        print('List Applications')
        found = False
        for university in self.app._universities:
            for faculty in university._faculties:
                if self.user in faculty._candidates:
                    found = True
                    print('{}, {}'.format(university, faculty))

        if not found:
            print('No applications found.')

    def register(self, name):
        BAC_score = input('BAC score: ')
        if not BAC_score:
            BAC_score = None
        user = User(name, BAC_score)
        self.app.add_user(user)
        return user

    def apply(self, **kwargs):
        print('Apply to faculty')
        university_name = input('University name: ')
        university = self.app.find_university(university_name)
        if university is None:
            print('No University found with the given name.')
            return

        faculty_name = input('Faculty name: ')
        faculty = university.find_faculty(faculty_name)
        if faculty is None:
            print('No Faculty found with the given name.')
            return

        try:
            faculty.apply(self.user)
        except ValueError as e:
            print(e)
            return
        print('Applied successfully.')

    def renounce(self, **kwargs):
        print('Renounce from faculty')
        university_name = input('University name: ')
        university = self.app.find_university(university_name)
        if university is None:
            print('No University found with the given name.')
            return

        faculty_name = input('Faculty name: ')
        faculty = university.find_faculty(faculty_name)
        if faculty is None:
            print('No Faculty found with the given name.')
            return

        try:
            faculty.renounce(self.user)
        except ValueError as e:
            print(e)
            return
        print('Renounce successful.')

    def quit(self, **kwargs):
        raise SystemExit('Exit')

    def debug(self, **kwargs):
        print('Debug')
        self.list_universities()
        print('List users')
        print(self.app._users)
