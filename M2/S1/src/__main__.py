"""
Usage:
script.py

To add universities/faculties:
script.py --admin

"""
import os
import pickle
import sys

from app import App, AdminMenu, UserMenu
from config import PERSISTENCE
from user import User


def main():
    if os.path.exists(PERSISTENCE):
        with open(PERSISTENCE, 'rb') as f:
            app = pickle.load(f)
    else:
        app = App()

    if len(sys.argv) > 1 and sys.argv[1] == '--admin':
        AdminMenu(app).loop()
    else:
        UserMenu(app).loop()


if __name__ == '__main__':
    main()
