import utils


class University:
    def __init__(self, name):
        if not name.strip():
            raise ValueError('Name must not be empty.')

        self.name = utils.normalize_name(name)
        self._faculties = []

    def __eq__(self, other):
        return self.name == other.name

    def __repr__(self):
        return 'University: {}'.format(self.name)

    def add_faculty(self, faculty):
        if faculty not in self._faculties:
            self._faculties.append(faculty)

    def find_faculty(self, name):
        normalized_name = utils.normalize_name(name)
        for faculty in self._faculties:
            if faculty.name == normalized_name:
                return faculty


class Faculty:
    def __init__(self, name, num_openings):
        if not name.strip():
            raise ValueError('Name must not be empty.')

        self.name = utils.normalize_name(name)
        self.num_openings = num_openings
        self._candidates = []

    def __eq__(self, other):
        return self.name == other.name

    def __repr__(self):
        return 'Faculty: {}'.format(self.name)

    def is_eligible(self, candidate):
        return candidate.BAC_score and candidate.BAC_score >= 5

    def apply(self, candidate):
        if not self.is_eligible(candidate):
            raise ValueError('Candidate is not eligible. (BAC score)')

        if candidate in self._candidates:
            raise ValueError('Candidate already applied.')

        self._candidates.append(candidate)

    def renounce(self, candidate):
        if not candidate in self._candidates:
            raise ValueError('Candidate not found in candidates list.')

        self._candidates.remove(candidate)

    def simulate_admission(self):
        pass
