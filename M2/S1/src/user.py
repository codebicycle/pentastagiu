import utils


class User:
    def __init__(self, name, BAC_score=None):
        if not name.strip():
            raise ValueError('Name must not be empty.')

        if BAC_score is not None:
            try:
                BAC_score = float(BAC_score)
            except ValueError:
                raise ValueError('Invalid BAC score.')

        self.name = utils.normalize_name(name)
        self.BAC_score = BAC_score

    def __eq__(self, other):
        return self.name == other.name

    def __repr__(self):
        return 'User: {}'.format(self.name)
